CREATE DATABASE chit;
USE chit;

CREATE TABLE `invoice` (
  `IID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IDate` datetime NOT NULL,
  `IConfirmDate` datetime DEFAULT NULL,
  `IAmount` int(10) unsigned NOT NULL DEFAULT '0',
  `IAmountPayed` int(10) unsigned DEFAULT NULL,
  `UID_Recipient` int(10) unsigned NOT NULL,
  `UID_Confirmer` int(10) unsigned NOT NULL,
  PRIMARY KEY (`IID`),
  KEY `UID_Recipient` (`UID_Recipient`),
  KEY `UID_Confirmer` (`UID_Confirmer`)
) ENGINE=InnoDB;

CREATE TABLE `log` (
  `LID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LType` tinyint(3) unsigned NOT NULL,
  `LData` text NOT NULL,
  `LDate` datetime NOT NULL,
  `U_ID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`LID`)
) ENGINE=InnoDB;

CREATE TABLE `product` (
  `PID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PName` varchar(64) NOT NULL,
  `PNumber` bigint(20) unsigned DEFAULT NULL,
  `PCost` int(10) unsigned NOT NULL DEFAULT '0',
  `PDeposit` int(10) unsigned NOT NULL DEFAULT '0',
  `PStock` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PID`),
  KEY `PNumber` (`PNumber`)
) ENGINE=InnoDB;

CREATE TABLE `trn` (
  `TID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TType` enum('restock','bought','costfix','stockfix','deposit') NOT NULL,
  `TCost` int(10) unsigned NOT NULL DEFAULT '0',
  `TStock` int(11) NOT NULL DEFAULT '0',
  `TBalance` int(11) NOT NULL DEFAULT '0',
  `TDate` datetime NOT NULL,
  `U_ID` int(10) unsigned NOT NULL,
  `P_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`TID`),
  KEY `U_ID` (`U_ID`),
  KEY `P_ID` (`P_ID`)
) ENGINE=InnoDB;

CREATE TABLE `user` (
  `UID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `UName` varchar(64) NOT NULL,
  `UStudentNumber` bigint(20) NOT NULL DEFAULT '0',
  `UPass` varchar(32) DEFAULT NULL,
  `URegDate` date DEFAULT NULL,
  `UBalance` int(11) NOT NULL DEFAULT '0',
  `UMail` varchar(96) NOT NULL,
  `UPerm` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `UConfirmDate` datetime DEFAULT NULL,
  `UOutDeposit` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`UID`),
  KEY `UStudentNumber` (`UStudentNumber`)
) ENGINE=InnoDB;


