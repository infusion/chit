<?php

class Chit {

    // Status flags
    const S_NO_PRODUCT = 1;
    const S_NO_USER = 2;
    const S_USER_EXISTS = 4;
    const S_PRODUCT_EXISTS = 8;
    const S_NO_DEPOSIT = 16;
    const S_NO_MORE_DEPOSIT = 32;
    // Log constants
    const L_ADDUSER = 1;
    const L_DELUSER = 2;
    const L_ADDPRODUCT = 3;
    const L_PRODUCT_NAME = 4;

    private $user = null;
    private $product = null;
    private $status = 0;

    public function init($product, $user, $is_id = false) {

        if ($user !== null) {
            $res = DB::query("SELECT * FROM user WHERE " . ($is_id ? "UID" : "UStudentNumber") . "=#i LIMIT 1", $user);
            $this->user = mysqli_fetch_assoc($res);
            mysqli_free_result($res);

            if (!isset($this->user, $this->user['UID'])) {
                $this->user = null;
                $this->status|= self::S_NO_USER;
            }
        }

        if ($product !== null) {
            $res = DB::query("SELECT * FROM product WHERE " . ($is_id ? "PID" : "PNumber") . "=#i LIMIT 1", $product);
            $this->product = mysqli_fetch_assoc($res);
            mysqli_free_result($res);

            if (!isset($this->product, $this->product['PID'])) {
                $this->product = null;
                $this->status|= self::S_NO_PRODUCT;
            }
        }
        return $this;
    }

    public function isStatus($stat) {
        return ($stat & $this->status) !== 0;
    }

    private function _trn($type) {
        DB::query("INSERT INTO trn "
                . "SELECT NULL, #s, PCost, PStock, (SELECT UBalance FROM user WHERE UID=#i), NOW(), #i, PID "
                . "FROM product "
                . "WHERE PID=#i LIMIT 1", $type, $this->user['UID'], $this->user['UID'], $this->product['PID']);
    }

    private function _log($type) {
        $args = func_get_args();
        array_shift($args);

        DB::query("INSERT INTO log SET LType=#i, LData=#s, LDate=NOW(), U_ID=#i", $type, json_encode($args), $this->user ? $this->user['UID'] : 0);
    }

    public function addUser($name, $email, $studid, $pass = null) {

        $row = mysqli_fetch_row(DB::query('SELECT 1 FROM user WHERE UMail=#s OR UStudentNumber=#s LIMIT 1', $email, $studid));

        if (isset($row[0])) {
            $this->status|= self::S_USER_EXISTS;
        } else {
            DB::query("INSERT INTO user SET UName=#s, UMail=#s, UStudentNumber=#i, URegDate=NOW()" . ($pass ? ", UPass='" . md5($pass) . "'" : ""), $name, $email, $studid);
            $this->_log(self::L_ADDUSER, DB::id(), $name, $email, $studid);
        }
    }

    public function buyProduct() {

        if ($this->status === 0) {
            DB::query("UPDATE product SET PStock=PStock-1 WHERE PID=#i LIMIT 1", $this->product['PID']);
            DB::query("UPDATE user SET UBalance=UBalance-#i, UOutDeposit=UOutDeposit+#i WHERE UID=#i LIMIT 1", $this->product['PCost'], $this->product['PDeposit'] > 0, $this->user['UID']);

            $this->_trn('bought');
        }
    }

    public function confirmUser($email) {
        // DB::query("UPDATE user SET UConfirmDate=NOW() WHERE UMail=#s LIMIT 1", $email);
        // $this->_log(self::L_ADDUSER, $name, $email, $studid, $pass); 
    }

    public function deleteUser() {
        if ($this->status === 0) {
            DB::query("DELETE FROM user WHERE UID=#i LIMIT 1", $this->user['UID']);
            // TODO: loggen wer gelöscht hat?
            $this->_log(self::L_DELUSER, $this->user['UID'], $this->user['UName'], $this->user['UMail'], $this->user['UStudentNumber']);
            return true;
        }
        return false;
    }

    public function changeProduct($field, $val) {
        if ($this->status === 0) {
            DB::query("UPDATE product SET " . $field . " WHERE PID=#i", $val, $this->product['PID']);

            if ($field === 'PName=#s')
                $this->_log(self::L_PRODUCT_NAME, $this->product['PID'], $this->product['PName'], $val, $this->user['UID']);
        }
    }

    public function addProduct($name, $number, $cost, $deposit) {

        $row = mysqli_fetch_row(DB::query('SELECT 1 FROM product WHERE PNumber=#i LIMIT 1', $number));

        if (isset($row[0])) {
            $this->status|= self::S_PRODUCT_EXISTS;
        } else {
            DB::query("INSERT INTO product SET PName=#s, PNumber=#i, PCost=#m, PDeposit=#m", $name, $number, $cost, $deposit);
            $this->_log(self::L_ADDPRODUCT, $name, $number, $cost, $deposit);
        }
    }

    public function correctProductPrice($cost) {
        if ($this->status === 0) {
            DB::query("UPDATE product SET PCost=#m WHERE PID=#i LIMIT 1", $cost, $this->product['PID']);

            $this->_trn('costfix');
            return true;
        }
        return false;
    }

    public function correctProductStock($stock) {
        if ($this->status === 0) {
            DB::query("UPDATE product SET PStock=#i WHERE PID=#i LIMIT 1", $stock, $this->product['PID']);

            $this->_trn('stockfix');
            return true;
        }
        return false;
    }

    public function restockProduct($num) {
        if ($this->status === 0) {
            DB::query("UPDATE product SET PStock = PStock + #i WHERE PID=#i LIMIT 1", $num, $this->product['PID']);

            $this->_trn('restock');
            return true;
        }
        return false;
    }

    public function depositProduct() {
        if ($this->status === 0) {

            /* / pfand quota
              SELECT COUNT(*)
              FROM trn
              WHERE TType = 'deposit' AND U_ID = 1 AND TDate > DATE_SUB(NOW(), INTERVAL 15 MINUTE)
              ORDER BY TDate DESC
             */



            if ($this->product['PDeposit'] > 0) {

                // nur pfand, wenn man vorher was mit pfand gekauft hat
                if ($this->user['UOutDeposit'] > 0) {

                    DB::query("UPDATE user SET UBalance = UBalance + #i,UOutDeposit=UOutDeposit-1 WHERE UID=#i LIMIT 1", $this->product['PDeposit'], $this->user['UID']);

                    $this->_trn('deposit');
                    return true;
                } else {
                    $this->status|= self::S_NO_MORE_DEPOSIT;
                }
            } else {
                $this->status|= self::S_NO_DEPOSIT;
            }
        }
        return false;
    }

}
