<?php

class DB {

    private static $a = 0;
    private static $args = null;
    private static $db = null;

    public static function id() {
        return self::$db->insert_id;
    }

    public static function affected() {
        return self::$db->affected_rows();
    }

    public static function query($query) {
        self::$args = func_get_args();
        $query = preg_replace_callback('/#([a-z])/i', 'self::_escape', $query);
        self::$a = 0;

        if (self::$db === null) {
            self::$db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        }
        return mysqli_query(self::$db, $query);
    }

    private static function _escape($c) {
        switch ($c[1]) {
            case 'i':
                return (int) self::$args[++self::$a];
            case 'm':
                return (int) ((float) self::$args[++self::$a] * 100);
            case 's':
                return "'" . addslashes(self::$args[++self::$a]) . "'";
        }
        return "NUT";
    }

}
