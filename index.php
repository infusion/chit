<?php

// TODO:
// geld einzahlen / rechnungen

function __autoload($class) {
    require './lib/' . strtolower($class) . '.php';
}

define('DB_HOST', 'localhost');
define('DB_USER', '***');
define('DB_PASS', '***');
define('DB_NAME', '**');

$C = new Chit;

// AJAX
if (isset($_GET['edit'])) {

    $user = 1;

    switch ($_GET['edit']) {
        case 'price':
            $C->init($_GET['id'], $user, true)->correctProductPrice($_GET['val']);
            break;
        case 'stock':
            $C->init($_GET['id'], $user, true)->correctProductStock($_GET['val']);
            break;
        case 'restock':
            $C->init($_GET['id'], $user, true)->restockProduct($_GET['val']);
            break;
        case 'delete':
            $C->init(null, $_GET['id'], true)->deleteUser();
            break;
        case 'name':
            $C->init($_GET['id'], $user, true)->changeProduct('PName=#s', $_GET['val']);
            break;
    }
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $_POST = array_map("trim", $_POST);

    if ($_POST['type'] === 'buy') {
        $C->init($_POST['product'], $_POST['user'])->buyProduct();
    } else if ($_POST['type'] === 'newuser') {
        $C->addUser($_POST['name'], $_POST['mail'], $_POST['studid']);
    } else if ($_POST['type'] === 'newproduct') {
        $C->addProduct($_POST['product'], $_POST['number'], $_POST['cost'], $_POST['deposit']);
    } else if ($_POST['type'] === 'deposit') {
        $C->init($_POST['product'], $_POST['user'])->depositProduct();
    }
}
?>
<html>

    <head>

        <style>

            body {
                margin: 0;
                background: #ddd;
            }

            #main {
                width:80%;
                margin:auto;
                background:#fff;
                padding: 10px;
            }


        </style>



    </head>

    <body>
        <div id="main">

            <header>
                <div style="color:#c00">
                    <?php
                    if ($C->isStatus(Chit::S_NO_USER)) {
                        echo "User doesn't exist!<br>";
                    }

                    if ($C->isStatus(Chit::S_NO_PRODUCT)) {
                        echo "Product doesn't exist!<br>";
                    }

                    if ($C->isStatus(Chit::S_USER_EXISTS)) {
                        echo "User already exists<br>";
                    }

                    if ($C->isStatus(Chit::S_PRODUCT_EXISTS)) {
                        echo "Product already exists<br>";
                    }

                    if ($C->isStatus(Chit::S_NO_MORE_DEPOSIT)) {
                        echo "No more deposit returns!<br>";
                    }


                    if ($C->isStatus(Chit::S_NO_DEPOSIT)) {
                        echo "No deposit for this product!<br>";
                    }
                    ?>
                </div>
            </header>

            <section>
                <h1>Produkte</h1>
                <ul>
                    <?php
                    $res = DB::query("SELECT * FROM product");
                    while (null !== ($row = mysqli_fetch_assoc($res))) {
                        echo '<li><b>', $row['PNumber'], '</b>: <span class="name" data-id="', $row['PID'], '">', $row['PName'], '</span> (<span class="price" data-id="', $row['PID'], '">', $row['PCost'] / 100, '</span>€, <span class="stock" data-id="', $row['PID'], '">', $row['PStock'], '</span> Stück, <a href="#" class="restock" data-id="', $row['PID'], '">Restock</a>)</li>';
                    }
                    ?>
                </ul>


                <h1>User</h1>
                <ul>
                    <?php
                    $res = DB::query("SELECT * FROM user");
                    while (null !== ($row = mysqli_fetch_assoc($res))) {
                        echo '<li><b>', $row['UStudentNumber'], '</b>: ', $row['UName'], ' (', $row['UBalance'] / 100, '€) <a class="delete" data-id="', $row['UID'], '" href="#">X</a></li>';
                    }
                    ?>
                </ul>


                <h1>Kaufen</h1>
                <form method="post" action="">
                    <input type="hidden" name="type" value="buy">
                    <input value="User" type="text" name="user">
                    <input value="Product" type="text" name="product">

                    <input type="submit" value="Kaufen">
                </form>

                <h1>Pfand zurück</h1>
                <form method="post" action="">
                    <input type="hidden" name="type" value="deposit">
                    <input value="User" type="text" name="user">
                    <input value="Product" type="text" name="product">

                    <input type="submit" value="Zurück">
                </form>

                <h1>User anlegen</h1>
                <form method="post" action="">
                    <input type="hidden" name="type" value="newuser">
                    <input value="Name" type="text" name="name">
                    <input value="Email" type="text" name="mail">
                    <input value="Matrikelnummer" type="text" name="studid">

                    <input type="submit" value="Anlegen">
                </form>

                <h1>Produkte anlegen</h1>
                <form method="post" action="">
                    <input type="hidden" name="type" value="newproduct">
                    <input value="Name" type="text" name="product">
                    <input value="Nummer" type="text" name="number">
                    <input value="Preis" type="text" name="cost">
                    <input value="Pfand" type="text" name="deposit">

                    <input type="submit" value="Anlegen">
                </form>



        </div>
    </section>

    <footer>


    </footer>

    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>

    <script>

        (function(window) {

            function _call(type, id, val) {

                $.ajax('/?edit=' + type + "&id=" + id + "&val=" + encodeURIComponent(val), function() {

                });
            }

            var $ = window.jQuery;

            $('.price').click(function() {

                var val = prompt("Neuer Preis", $(this).html());
                if (val === null || val < 0)
                    return;

                $(this).html(val);
                _call('price', $(this).data("id"), val);
            });

            $('.stock').click(function() {

                var val = prompt("Neue Anzahl", $(this).html());
                if (val === null || val < 0)
                    return;

                $(this).html(val);
                _call('stock', $(this).data("id"), val);
            });

            $('.restock').click(function() {

                var val = prompt("Anzahl gekauft", 0);
                if (val === null || val <= 0)
                    return;

                $(".stock[data-id=" + $(this).data("id") + "]").html(
                        parseInt($(".stock[data-id=" + $(this).data("id") + "]").html()) + parseInt(val)
                        );
                _call('restock', $(this).data("id"), val);
            });

            $('.delete').click(function(ev) {
                ev.preventDefault();

                $(this).parent().remove();
                _call('delete', $(this).data("id"), "");
            });

            $('.name').click(function(ev) {

                var val = prompt("Neuer Name", $(this).html());
                if (val === null)
                    return;

                $(this).html(val);
                _call('name', $(this).data("id"), val);

            });



        })(this);


    </script>

</body>

</html>
